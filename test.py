import cv2
import numpy as np


lower = np.array([89, 120, 168])
upper = np.array([95, 255, 255])

img = cv2.imread('test4.png')
screen_temp = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
mask = cv2.inRange(screen_temp, lower, upper)
contours, hier = cv2.findContours(mask, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
for c in contours:
    # find bounding box coordinates
    x, y, w, h = cv2.boundingRect(c)
    cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
    # calculate coordinates of the minimum area rectangle
print(len(contours))
cv2.drawContours(img, contours, -1, (255, 0, 0), 1)
while True:
    cv2.imshow("contours", img)
    if cv2.waitKey(10) & 0xFF == ord('q'):
        break