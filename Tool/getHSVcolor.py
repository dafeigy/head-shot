#该程序用于搭配滤色函数使用，目的是为了确定HSV颜色空间的范围，运行后会打开目标图片以及转换得到的HSV空间的图片
#通过鼠标点击HSV空间的图片中的像素可以得到该点的HSV，采样得到HSV的最大最小值，在滤色函数中填入upper和lower的数组。
#e.g.
# lower = np.array([89, 142, 168]) #对应HSV最小值
# upper = np.array([95, 250, 255]) #对应HSV最大值
import cv2
import numpy as np
from matplotlib import pyplot as plt

image=cv2.imread(r'G:\head-shot\MutilTarget.png')
screen_gray = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
def getpos(event,x,y,flags,param):
    if event==cv2.EVENT_LBUTTONDOWN: #定义一个鼠标左键按下去的事件
        print(screen_gray[y,x])

cv2.imshow("imageHSV",screen_gray)
cv2.imshow('image',image)
cv2.setMouseCallback("imageHSV",getpos)
cv2.waitKey(0)