#该程序用于裁切照片
from PIL import Image
import os
import os.path
import numpy as np
import cv2

# 指明被遍历的文件夹
rootdir = r'F:\Headshot\Img\test'
for parent, dirnames, filenames in os.walk(rootdir):  # 遍历当前文件夹下的每一张图片
    for i in range(len(filenames)):
        print('parent is :' + parent)
        print('filename is :' + filenames[i])
        currentPath = os.path.join(parent, filenames[i])
        print('the fulll name of the file is :' + currentPath)

        img = Image.open(currentPath)
        print(img.format, img.size, img.mode)
        # img.show()
        box1 = (0, 230, 1280, 550)  # 设置左、上、右、下的像素
        image1 = img.crop(box1)  # 图像裁剪
        image1.save(r"F:\Headshot\Img\train" + '\\' + "{}.jpg".format(i))  # 存储裁剪得到的图像