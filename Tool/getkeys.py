import win32api as wapi
import time

keyList = ["\b"]
for char in "ABCDEFGHIJKLMNOPQRSTUVWXYZ 123456789,.'£$/\\":
    keyList.append(char)

def key_check():
    keys = []
    for key in keyList:
        if wapi.GetAsyncKeyState(ord(key)):
            keys.append(key)
    return keys

def get_key(keys):
    output = [0,0,0,0,0,0]
    if 'A' in keys:
        output[0] = 1
    elif 'D' in keys:
        output[1] = 1
    elif 'Z' in keys:
        output[2] = 1
    elif 'J' in keys:
        output[3] = 1
        print('\033[0;36m捕捉到跳\033[0m')
    elif 'P' in keys:
        output[4] = 1
        print('\033[0;34m捕捉到冲\033[0m')
    elif 'V' in keys:#停止记录操作
        output = [1,1,1,1,1,1]
    else:
        output[5]=1
    return output

if __name__ == '__main__':
    while True:
        if get_key(key_check()) != [1,1,1,1,1,1]:
            print(key_check())
        else:
            break