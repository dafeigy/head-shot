#灰度化图像处理
import cv2
import os.path
# 指明被遍历的文件夹
rootdir = r'F:\Analyse'
# 输出文件夹目录
outputdir=r'F:\Analyse\test\\{}'
for parent, dirnames, filenames in os.walk(rootdir):  # 遍历当前文件夹下的每一张图片,只用关注filenames和parent就好
    for i in range(len(filenames)):
        currentPath = os.path.join(parent, filenames[i])
        temp = cv2.imread(currentPath)
        #核心
        gray=cv2.cvtColor(temp,cv2.COLOR_BGR2GRAY)
        #核心结束
        cv2.imwrite(outputdir.format(filenames[i]),gray)