#用于叠加图片输入,这里以两个滤色结果叠加为例
#所有以cv2.imread读取的对象都可以叠加
import cv2
import  numpy as np
import os.path
#四个HSV阈值
low_hsv_r = np.array([8,215,134])
high_hsv_r = np.array([19,253,255])
low_hsv_b = np.array([0,0,255])
high_hsv_b = np.array([109,23,255])
# 指明被遍历的文件夹
rootdir = r'F:\Analyse'
# 输出文件夹目录
outputdir=r'F:\Analyse\test\\{}'
for parent, dirnames, filenames in os.walk(rootdir):  # 遍历当前文件夹下的每一张图片,只用关注filenames和parent就好
    for i in range(len(filenames)):
        currentPath = os.path.join(parent, filenames[i])
        temp = cv2.imread(currentPath)
        #核心
        screen_gray = cv2.cvtColor(temp, cv2.COLOR_BGR2HSV)  # HSV收集
        screen_gray1 = cv2.bitwise_not(cv2.inRange(screen_gray, lowerb=low_hsv_r, upperb=high_hsv_r))#筛选红色，同时反转筛选区域（bitwisw—not），红色部分会变黑，背景是白色
        screen_gray2 = (cv2.inRange(screen_gray, lowerb=low_hsv_b, upperb=high_hsv_b))#筛选蓝色，蓝色部分会被标白出来，北京市黑色
        screen_add = cv2.addWeighted(screen_gray1, 0.3, screen_gray2, 0.7, 10)# 图像叠加，0.3，0.7分别是权重，最后一个参数Gamma可以理解成缓和程度
        #核心结束
        cv2.imwrite(outputdir.format(filenames[i]),screen_add)
