#用于滤色，根据HSV空间滤色，最好getHSVcolor使用
import cv2
import os
import numpy as np
import time
lower = np.array([78, 43, 46])    #HSV颜色下线,这里是一段蓝色下限
upper = np.array([124, 255, 255])    #HSV颜色上限，这里是一段蓝色上限
rootdir = r'F:\Analyse'
# 输出文件夹目录
outputdir=r'F:\Analyse\mask\\{}'
for parent, dirnames, filenames in os.walk(rootdir):  # 遍历当前文件夹下的每一张图片,只用关注filenames和parent就好
    for i in range(len(filenames)):
        currentPath = os.path.join(parent, filenames[i])
        temp = cv2.imread(currentPath)  #读取图片
        #####################核心部分####################
        screen_temp = cv2.cvtColor(temp, cv2.COLOR_BGR2HSV)#切换到HSV空间
        mask = cv2.inRange(screen_temp, lower, upper)   #设置HSV滤色遮罩
        #########################核心结束################
        cv2.imshow('test',mask)
        if cv2.waitKey(1000) & 0xFF == ord('q'):
            break

